import {Component, OnInit} from '@angular/core';
import {LoginService} from '../login.service';
import {Credentials} from '../entities/credentials';
import {User} from '../entities/user';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AuthGuard} from '../../shared/guards/auth.guard';


@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent{
    private tag = '[LoginFormComponent]';

    protected credentials: Credentials = new Credentials();

    constructor(private loginService: LoginService, private router: Router, private authGuard: AuthGuard) {
    }

    submit() {
        this.loginService
            .authenticate(this.credentials)
            .pipe(
                tap((user: User) => {
                    console.log(this.tag, 'Logged user', user);
                    this.router.navigate(['/']);
                })
            )
            .subscribe();
    }

}
