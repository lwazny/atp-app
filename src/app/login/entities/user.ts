export class User {
    username: string;
    password: string;

    static of(username: string, password: string): User {
        const user = new User();
        user.username = username;
        user.password = password;

        return user;
    }

    static fromJson(data: Object): User {
        const username = data['username'] || null;
        const password = data['password'] || null;

        if (username === null) {
            return null;
        }

        const user = User.of(username, password);

        return user;
    }
}
