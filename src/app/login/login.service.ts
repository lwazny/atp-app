import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';

import {User} from './entities/user';
import {Credentials} from './entities/credentials';
import {StorageService} from '../shared/storage.service';

@Injectable()
export class LoginService {
    private tag = '[LoginService]';
    private KEY_CURRENT_USER = 'currentUser';

    constructor(protected http: HttpClient, protected storageService: StorageService) {
    }

    public authenticate(credentials: Credentials): Observable<User> {
        const user: User = User.of(credentials.username, credentials.password);

        this.storageService.sessionSet(this.KEY_CURRENT_USER, user);
        return of(user);
    }
}
