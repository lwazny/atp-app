import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {LoginFormComponent} from './login-form/login-form.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {LoginService} from './login.service';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        HttpClientModule,
        LoginRoutingModule
    ],
    declarations: [LoginFormComponent],
    providers: [LoginService],

})
export class LoginModule {
}
