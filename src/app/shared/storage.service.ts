import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root',
})
export class StorageService {
    private tag = '[StorageService]';

    constructor() {
        console.log(this.tag, 'constructor');
    }

    public sessionHas(key: string): boolean {
        return sessionStorage.getItem(key) !== null;
    }

    public sessionGet(key: string): any {
        if (!this.sessionHas(key)) {
            throw new Error('Key not exist');
        }

        return JSON.parse(sessionStorage.getItem(key));
    }

    public sessionSet(key: string, value: any) {
        return sessionStorage.setItem(key, JSON.stringify(value));
    }
}
