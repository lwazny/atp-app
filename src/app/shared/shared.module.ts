import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StorageService} from './storage.service';
import {AuthGuard} from './guards/auth.guard';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [],
    providers: [
        AuthGuard,
        StorageService
    ],

})
export class SharedModule {
}
