import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route} from '@angular/router';
import {StorageService} from '../storage.service';
import {User} from '../../login/entities/user';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad {
    private tag = '[AuthGuard]';

    private KEY_CURRENT_USER = 'currentUser';

    constructor(private router: Router, private storageService: StorageService) {
    }

    isAuthenticated() {
        try {
            const user: User = User.fromJson(this.storageService.sessionGet(this.KEY_CURRENT_USER));
            return user instanceof User;
        } catch (e) {
            return false;
        }
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const requireAuth = route.data.requireAuth || false;

        if (requireAuth && this.isAuthenticated()) {
            if(route.routeConfig.path === ''){
                this.router.navigate(['/features']);

            }

            return true;
        } else {
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
        }

        return false;
    }

    canLoad(route: Route){
        const requireAuth = route.data.requireAuth || false;

        if (requireAuth && this.isAuthenticated()) {
            return true;
        } else {
            this.router.navigate(['/login']);
        }
    }
}
