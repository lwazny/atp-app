import {Injectable} from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {BitbayClient} from './bitbay.service';
import {map, tap} from 'rxjs/operators';
import {Ticker} from './entities/ticker';
import {forkJoin, Observable} from 'rxjs';

@Injectable()
export class TickerResolve implements Resolve<any> {
    constructor(private  bitbayClient: BitbayClient) {
    }

    protected btcpln: Ticker = new Ticker();
    protected lskpln: Ticker = new Ticker();
    protected ethpln: Ticker = new Ticker();

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

        let btcpln = this.bitbayClient.getTicker('BTC', 'PLN').pipe(tap((ticker: Ticker) => this.btcpln = ticker ));
        let lskpln = this.bitbayClient.getTicker('LSK', 'PLN').pipe(tap((ticker: Ticker) => this.lskpln = ticker ));
        let ethpln = this.bitbayClient.getTicker('ETH', 'PLN').pipe(tap((ticker: Ticker) => this.ethpln = ticker ));

        return forkJoin(
            btcpln,
            lskpln,
            ethpln
        ).pipe(map(result => {
            return {
                btcpln: result[0],
                lskpln: result[1],
                ethpln: result[2]
            }
        }));
    }
}
