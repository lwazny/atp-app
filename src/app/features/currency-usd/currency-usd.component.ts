import {Component, Input, OnInit, TemplateRef} from '@angular/core';

@Component({
    selector: 'app-currency-usd',
    templateUrl: './currency-usd.component.html',
    styleUrls: ['./currency-usd.component.css']
})
export class CurrencyUsdComponent implements OnInit {

    @Input()
    public currencyName;

    @Input()
    public plnValue;

    @Input()
    template: TemplateRef<any>;

    public ctx = {
        currencyName: this.currencyName,
        usdValue: this.usdValue
    };

    get usdValue() {
        return this.plnValue * 3;
    }

    constructor() {
    }

    ngOnInit() {
    }

}
