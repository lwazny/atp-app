import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-currency-pln',
    templateUrl: './currency-pln.component.html',
    styleUrls: ['./currency-pln.component.css']
})
export class CurrencyPlnComponent implements OnInit {

    @Input()
    public currencyName;

    constructor() {
    }

    ngOnInit() {
    }

}
