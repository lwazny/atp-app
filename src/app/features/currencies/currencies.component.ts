import {Component, OnInit} from '@angular/core';
import {Ticker} from '../entities/ticker';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-currencies',
    templateUrl: './currencies.component.html',
    styleUrls: ['./currencies.component.css']
})
export class CurrenciesComponent implements OnInit {

    protected tickers: {
        btcpln: Ticker
        lskpln: Ticker
        ethpln: Ticker
    };

    constructor(private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.tickers = this.route.snapshot.data['tickers'];
    }

}
