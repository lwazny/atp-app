import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CurrenciesComponent} from './currencies/currencies.component';
import {FeaturesComponent} from './features/features.component';
import {TickerResolve} from './currencies.resolve';

const routes: Routes = [
    {
        path: '',
        component: FeaturesComponent,
        children: [
            {
                path: 'currencies',
                component: CurrenciesComponent,
                resolve:{
                    tickers: TickerResolve
                }
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesRoutingModule { }
