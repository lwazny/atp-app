export class Ticker {
    public max: number = 0;
    public min: number = 0;
    public last: number = 0;
    public bid: number = 0;
    public ask: number = 0;
    public vwap: number = 0;
    public average: number = 0;
    public volume: number = 0;

    static fromJson(data: any){
        const ticker = new Ticker();

        ticker.max = data.max;
        ticker.min = data.min;
        ticker.last = data.last;
        ticker.bid = data.bid;
        ticker.ask = data.ask;
        ticker.vwap = data.vwap;
        ticker.average = data.average;
        ticker.volume = data.volume;

        return ticker;
    }
}
