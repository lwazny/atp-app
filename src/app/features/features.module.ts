import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { CurrenciesComponent } from './currencies/currencies.component';
import { FeaturesComponent } from './features/features.component';
import {BitbayClient} from './bitbay.service';
import { CurrencyPlnComponent } from './currency-pln/currency-pln.component';
import { CurrencyUsdComponent } from './currency-usd/currency-usd.component';
import { DecimalPipe } from './decimal.pipe';
import {TickerResolve} from './currencies.resolve';

@NgModule({
  imports: [
    CommonModule,
    FeaturesRoutingModule
  ],
  declarations: [CurrenciesComponent, FeaturesComponent, CurrencyPlnComponent, CurrencyUsdComponent, DecimalPipe],
    providers:[TickerResolve, BitbayClient]
})
export class FeaturesModule { }
