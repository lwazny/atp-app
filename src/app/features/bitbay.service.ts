import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

import {Ticker} from './entities/ticker';
import {Observable} from 'rxjs';

@Injectable()
export class BitbayClient {
    private tag = '[BitbayService]';
    private TICKER = 'https://bitbay.net/API/Public/${currency1}${currency2}/${ticker}.json';

    constructor(protected http: HttpClient) {
        console.log(this.tag, 'constructor');
    }

    public getTicker(currency1: string, currency2: string): Observable<Ticker> {
        const endpoint = this.interpolate(this.TICKER, {currency1, currency2, ticker: 'ticker'});

        return this.http.get(endpoint).pipe(
            map((response: any) => {
                return Ticker.fromJson(response);
            })
        );
    }

    private interpolate(template: string, params: {}) {
        const names = Object.keys(params);
        const vals = Object.values(params);
        return new Function(...names, `return \`${template}\`;`)(...vals);
    }
}
