import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AuthGuard} from './shared/guards/auth.guard';

const routes: Routes = [
    {path: 'login', loadChildren: './login/login.module#LoginModule'},
    {path: 'features', loadChildren: './features/features.module#FeaturesModule', data: {requireAuth: true}, canActivate: [AuthGuard], canLoad: [AuthGuard]},
    {path: '', component: HomeComponent, data: {requireAuth: true}, canActivate: [AuthGuard]},

];

@NgModule({
    imports: [RouterModule.forRoot(routes, {enableTracing: false})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
